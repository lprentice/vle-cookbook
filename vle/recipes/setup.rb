#
# Taken from:
# http://docs.aws.amazon.com/opsworks/latest/userguide/gettingstarted.walkthrough.photoapp.3.html
#
node[:deploy].each do |app_name, deploy|
  template "#{deploy[:deploy_to]}/application/config/database.php" do
    source "database.php.erb"
    mode 0660
    group deploy[:group]

    variables(
      :hostname =>     (node[:deploy]['vle'][:database][:host] rescue nil),
      :username =>     (node[:deploy]['vle'][:database][:username] rescue nil),
      :password => (node[:deploy]['vle'][:database][:password] rescue nil),
      :database =>       (node[:deploy]['vle'][:database][:database] rescue nil),
      :prefix =>    (node[:phpapp][:prefix] rescue nil),
      :debug =>       (node[:database][:debug] rescue nil)
    )

   only_if do
     File.directory?("#{deploy[:deploy_to]}")
   end
  end
end 